var request = require('request');

exports.getbuilding = (req, res, next) => {
	// let body = req.body;
	req.getConnection(function(err, connection) {
		connection.query(`select * from building`, function(err, result) {
			if (err) res.send(err);
			res.send({ success: true, result: result });
			next();
		});
	});
};
exports.getclassroom = (req, res, next) => {
	// let body = req.body;
	req.getConnection(function(err, connection) {
		connection.query(`select * from classroom`, function(err, result) {
			if (err) res.send(err);
			res.send({ success: true, result: result });
			next();
		});
	});
};
exports.gettool = (req, res, next) => {
	// let body = req.body;
	req.getConnection(function(err, connection) {
		connection.query(`select * from tool where type = 'tool' and status_t ='open'`, function(err, result) {
			if (err) res.send(err);
			res.send({ success: true, result: result });
			next();
		});
	});
};
exports.getrepair = (req, res, next) => {
	// let body = req.body;
	req.getConnection(function(err, connection) {
		connection.query(
			`select r.id as rpid, r.device_problem,r.date,r.status,r.detail,r.type,c.cname,u.fname,u.lname 
		from repair r join user u on r.uid=u.id join classroom c on r.cid=c.id`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.getrequest = (req, res, next) => {
	// let body = req.body;
	req.getConnection(function(err, connection) {
		connection.query(
			`select r.id as rqid, r.device_problem,r.date,r.status,r.detail,r.type,r.amount,c.cname,u.fname,u.lname 
		from request r join user u on r.uid=u.id join classroom c on r.cid=c.id`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.draw_request = (req, res, next) => {
	// let body = req.body;
	req.getConnection(function(err, connection) {
		connection.query(
			`SELECT d.tid,d.date,d.amount,d.type,t.tname,u.fname,u.lname
			FROM draw_request d join tool t on d.tid=t.id join user u on d.mid=u.id`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.draw_repair = (req, res, next) => {
	// let body = req.body;
	req.getConnection(function(err, connection) {
		connection.query(
			`SELECT d.tid,d.date,d.amount,d.type,t.tname,u.fname,u.lname
			FROM draw_repair d join tool t on d.tid=t.id join user u on d.mid=u.id`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.noti_line = (req, res, next) => {
	var token = req.body.token;
	var message = req.body.message;
	request(
		{
			method: 'POST',
			uri: 'https://notify-api.line.me/api/notify',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			auth: {
				bearer: token
			},
			form: {
				message: message
			}
		},
		(err, httpResponse, body) => {
			if (err) {
				console.log(err);
			} else {
				res.send({
					body: JSON.parse(body)
				});
				next();
			}
		}
	);
};
exports.get_user_by_id = (req, res, next) => {
	let body = req.body;
	req.getConnection(function(err, connection) {
		connection.query(`select line_token from user where id='${body.id}'`, function(err, result) {
			if (err) res.send(err);
			res.send({ success: true, result: result });
			next();
		});
	});
};
