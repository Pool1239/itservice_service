exports.getrepair_man = (req, res, next) => {
	// let body = req.body;
	req.getConnection(function(err, connection) {
		connection.query(
			`select *,r.id as rpid,r.status as status from repair r join classroom c on r.cid=c.id
		 where r.status = 'รอดำเนินการ' order by date DESC`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.getrepair_man2 = (req, res, next) => {
	let body = req.body;
	req.getConnection(function(err, connection) {
		connection.query(
			`select *,r.id as rpid,r.status as status from repair r join classroom c on r.cid=c.id join user u on r.uid = u.id
		 where r.status = 'กำลังดำเนินการ' and mid = '${body.mid}' order by date DESC`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.getrepair_man_success = (req, res, next) => {
	let body = req.body;
	req.getConnection(function(err, connection) {
		connection.query(
			`select *,r.id as rpid,r.status as status from repair r join classroom c on r.cid=c.id join user u on r.uid = u.id
		 where r.status = 'เสร็จสิ้น' and mid = '${body.mid}' order by date DESC`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.getrepair_man_by_id = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`select r.uid,r.id as rpid,m.fname as fname_m,m.lname as lname_m,date,r.status as status,type,cname,u.fname as fname_u,u.lname as lname_u,u.position,u.branch,u.phone,device_problem,detail
			from repair r join classroom c on r.cid=c.id join user u on r.uid=u.id join user m on r.mid=m.id
			where r.id = '${body.id}' order by date DESC`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.getrequest_man = (req, res, next) => {
	// let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`select *,r.id as rqid,r.status as status from request r join classroom c on r.cid=c.id
		 where r.status = 'รอดำเนินการ' order by date DESC`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.getrequest_man2 = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`select *,r.id as rqid,r.status as status from request r join classroom c on r.cid=c.id join user u on r.uid = u.id
		 where r.status = 'กำลังดำเนินการ' and mid = '${body.mid}' order by date DESC`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.getrequest_man_success = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`select *,r.id as rqid,r.status as status from request r join classroom c on r.cid=c.id join user u on r.uid = u.id
		 where r.status = 'เสร็จสิ้น' and mid = '${body.mid}' order by date DESC`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.getrequest_man_by_id = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`select r.uid,r.id as rpid,m.fname as fname_m,m.lname as lname_m,date,r.status as status,type,cname,u.fname as fname_u,u.lname as lname_u,u.position,u.branch,u.phone,device_problem,detail,amount
			from request r join classroom c on r.cid=c.id join user u on r.uid=u.id join user m on r.mid=m.id
			where r.id = '${body.id}' order by date DESC`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.updatestatus = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`update repair set mid = '${body.mid}', status = 'กำลังดำเนินการ' where id = '${body.id}'`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'สำเร็จ' });
				next();
			}
		);
	});
};
exports.updatestatus2 = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(`update repair set mid = null,status = 'รอดำเนินการ' where id = '${body.id}'`, function(
			err,
			result
		) {
			if (err) res.send(err);
			res.send({ success: true, message: 'สำเร็จ' });
			next();
		});
	});
};
exports.repair_success = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(`update repair set status = 'เสร็จสิ้น' where id = '${body.id}'`, function(err, result) {
			if (err) res.send(err);
			res.send({ success: true, message: 'สำเร็จ' });
			next();
		});
	});
};
exports.request_success = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(`update request set status = 'เสร็จสิ้น' where id = '${body.id}'`, function(err, result) {
			if (err) res.send(err);
			res.send({ success: true, message: 'สำเร็จ' });
			next();
		});
	});
};
exports.updatestatus3 = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`update request set mid = '${body.mid}',status = 'กำลังดำเนินการ' where id = '${body.id}'`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'สำเร็จ' });
				next();
			}
		);
	});
};
exports.updatestatus4 = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(`update request set mid = null,status = 'รอดำเนินการ' where id = '${body.id}'`, function(
			err,
			result
		) {
			if (err) res.send(err);
			res.send({ success: true, message: 'สำเร็จ' });
			next();
		});
	});
};
exports.gettools = (req, res, next) => {
	// let body = req.body;
	req.getConnection(function(err, connection) {
		connection.query(`select * from tool where status_t ='open'`, function(err, result) {
			if (err) res.send(err);
			res.send({ success: true, result: result });
			next();
		});
	});
};

exports.insertdraw_repair = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`insert into draw_repair(did,tid,mid,date,amount,type)
		values('${body.did}','${body.tid}','${body.mid}','${body.date}','${body.amount}','${body.type}')`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'สำเร็จ' });
				next();
			}
		);
	});
};
exports.updatedraw_repair = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`update draw_repair set did='${body.did}',tid='${body.tid}',mid='${body.mid}',date='${body.date}',amount='${body.amount}',type='${body.type}'
			where did='${body.did}' and tid='${body.tid}' and mid='${body.mid}'`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'สำเร็จ' });
				next();
			}
		);
	});
};
exports.deldraw_repair = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`delete from draw_repair where did='${body.did}' and tid='${body.tid}' and mid='${body.mid}'`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'สำเร็จ' });
				next();
			}
		);
	});
};
exports.getdraw_repair = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`select tid,d.amount,d.type,tname from draw_repair d join tool t on d.tid=t.id where did = '${body.did}'`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.getdraw_repair_manid = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`select tid,d.amount,d.type,tname,d.date from draw_repair d join tool t on d.tid=t.id where mid = '${body.id}'`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.insertdraw_request = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`insert into draw_request(did,tid,mid,date,amount,type)
		values('${body.did}','${body.tid}','${body.mid}','${body.date}','${body.amount}','${body.type}')`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'สำเร็จ' });
				next();
			}
		);
	});
};
exports.updatedraw_request = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`update draw_request set did='${body.did}',tid='${body.tid}',mid='${body.mid}',date='${body.date}',amount='${body.amount}',type='${body.type}'
			where did='${body.did}' and tid='${body.tid}' and mid='${body.mid}'`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'สำเร็จ' });
				next();
			}
		);
	});
};
exports.deldraw_request = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`delete from draw_request where did='${body.did}' and tid='${body.tid}' and mid='${body.mid}'`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'สำเร็จ' });
				next();
			}
		);
	});
};
exports.getdraw_request = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`select tid,d.amount,d.type,tname from draw_request d join tool t on d.tid=t.id where did = '${body.did}'`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.getdraw_request_manid = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`select tid,d.amount,d.type,tname,d.date from draw_request d join tool t on d.tid=t.id where mid = '${body.id}'`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.update_tools = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(`update tool set amount='${body.amount}' where id='${body.id}'`, function(err, result) {
			if (err) res.send(err);
			res.send({ success: true, message: 'สำเร็จ' });
			next();
		});
	});
};
