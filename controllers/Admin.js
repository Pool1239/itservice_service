exports.user_all = (req, res, next) => {
	// let body = req.body;
	req.getConnection(function(err, connection) {
		connection.query(`select * from user where role != 'close'`, function(err, result) {
			if (err) res.send(err);
			res.send({ success: true, result: result });
			next();
		});
	});
};
exports.del_user = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`update user set role='close'
			where id='${body.id}'`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'สำเร็จ' });
				next();
			}
		);
	});
};
exports.insert_user = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`insert into user(role,fname,lname,position,branch,phone,email,username,password,prefix,line_token)
		values('${body.role}','${body.fname}','${body.lname}','${body.position}','${body.branch}','${body.phone}','${body.email}','${body.username}','${body.password}','${body.prefix}','${body.line_token}')`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'สำเร็จ' });
				next();
			}
		);
	});
};
exports.update_user = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`update user set username = '${body.username}',password = '${body.password}',fname = '${body.fname}'
			,lname= '${body.lname}',position = '${body.position}',branch = '${body.branch}',phone= '${body.phone}'
			,email = '${body.email}',role = '${body.role}',prefix = '${body.prefix}',line_token = '${body.line_token}' where id = '${body.id}'`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'แก้ไขเสร็จสิ้น' });
				next();
			}
		);
	});
};
exports.insert_building = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(`insert into building(bname,status) values('${body.bname}','open')`, function(err, result) {
			if (err) res.send(err);
			res.send({ success: true, message: 'สำเร็จ' });
			next();
		});
	});
};
exports.del_building = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`update building set status='close'
			where id='${body.id}'`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'สำเร็จ' });
				next();
			}
		);
	});
};
exports.update_building = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`update building set bname='${body.bname}'
			where id='${body.id}'`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'สำเร็จ' });
				next();
			}
		);
	});
};
exports.del_room = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`update classroom set status='close'
			where id='${body.id}'`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'สำเร็จ' });
				next();
			}
		);
	});
};
exports.insert_room = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`insert into classroom(cname,bid,status) values('${body.cname}','${body.bid}','open')`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'สำเร็จ' });
				next();
			}
		);
	});
};

exports.update_room = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`update classroom set cname='${body.cname}'
			where id='${body.id}'`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'สำเร็จ' });
				next();
			}
		);
	});
};
exports.tool = (req, res, next) => {
	// let body = req.body;
	req.getConnection(function(err, connection) {
		connection.query(`select * from tool where status_t ='open'`, function(err, result) {
			if (err) res.send(err);
			res.send({ success: true, result: result });
			next();
		});
	});
};
exports.update_tool = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`update tool set amount='${body.amount}'
			where id='${body.id}'`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'สำเร็จ' });
				next();
			}
		);
	});
};
exports.insert_draw_tool = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`insert into draw_tool(tid,uid,date,amount) values('${body.tid}','${body.uid}','${body.date}','${body.amount}')`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'สำเร็จ' });
				next();
			}
		);
	});
};
exports.draw_tool = (req, res, next) => {
	// let body = req.body;
	req.getConnection(function(err, connection) {
		connection.query(
			`SELECT d.tid,d.date,d.amount,t.type,t.tname,u.fname,u.lname
			FROM draw_tool d join tool t on d.tid=t.id join user u on d.uid=u.id`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.insert_tool = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`insert into tool(tname,type,amount,status_t) values('${body.tname}','${body.type}','${body.amount}','open')`,
			function(err, result) {
				if (err) res.send(err);
				var base64Data = body.image;
				require('fs').appendFile('image/tool/' + result.insertId + '.png', base64Data, 'base64', function(err) {
					res.send({ success: true, message: 'เพิ่มรายการเสร็จสิ้น', id: result.insertId });
					next();
				});
			}
		);
	});
};
exports.del_tool = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`update tool set status_t='close'
			where id='${body.id}'`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'ลบสำเร็จ' });
				next();
			}
		);
	});
};
exports.update_tool_all = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`update tool set tname='${body.tname}',type='${body.type}'
			where id='${body.id}'`,
			function(err, result) {
				if (err) res.send(err);
				if (!err && body.image !== '')
					require('fs').writeFile('image/tool/' + body.id + '.png', body.image, 'base64', function(err) {});
				res.send({ success: true, message: 'แก้ไขสำเร็จ' });
				next();
			}
		);
	});
};
