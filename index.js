const express = require('express');
const app = express();
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const myConnection = require('express-myconnection');
const config = require('./config');
const routes = require('./routes');
const PORT = 3202;
const PART = '/api/v1';

app.use(function(req, res, next) {
	// Website you wish to allow to connect
	// res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
	// res.setHeader('Access-Control-Allow-Origin', 'http://35.240.227.156');
	res.setHeader('Access-Control-Allow-Origin', 'http://itserviceudru.com');
	res.setHeader('Access-Control-Allow-Methods', 'get, post');
	res.setHeader(
		'Access-Control-Allow-Headers',
		'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token,Authorization, X-Access-Token'
	);
	res.setHeader('Access-Control-Allow-Credentials', true);
	// Pass to next layer of middleware
	next();
});

app.use(bodyParser.urlencoded({ extended: true, limit: '100mb' }));
app.use(bodyParser.json({ limit: '100mb' }));

app.use(myConnection(mysql, config.dbOptions, 'pool'));

app.use(PART + '/image/repair', express.static(path.join(__dirname, 'image/repair')));
app.use(PART + '/image/tool', express.static(path.join(__dirname, 'image/tool')));

routes(app, PART);

app.listen(PORT, () => {
	console.log('localhost:' + PORT + PART);
});
